import { getField, updateField } from 'vuex-map-fields';

export const state = () => ({
  afterTransaction: {
    status: false,
    id: 0
  }
});

export const mutations = {
  updateField
};

export const actions = {

};

export const getters = {
  getField
};
