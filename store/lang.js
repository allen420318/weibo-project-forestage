import { getField, updateField } from 'vuex-map-fields';
import langData from '~/langData/langData';
import responsible from '~/langData/responsible';
import provacy from '~/langData/provacy';
import about from '~/langData/about';
import contact from '~/langData/contact';
import bank from '~/langData/bank';
import rules from '~/langData/rules';
import usdtPay from '~/langData/usdtPay';
import { getLangList } from '~/api/lang.js';

// const langList = [
//   { name: 'hk', show: '繁體' },
//   { name: 'en', show: 'English' },
//   { name: 'ja', show: '日本語' },
//   { name: 'th', show: 'ภาษาไทย' }
// ];

export const state = () => ({
  selectLang: '',
  langList: [],
  langObject: {},
  langData: {
    ...responsible,
    ...provacy,
    ...about,
    ...contact,
    ...bank,
    ...rules,
    ...usdtPay,
    ...langData
  }
});
export const getters = {
  getField
};
export const mutations = {
  updateField,
  setLangList (state, payload) {
    state.langList = payload;
  },
  setLangObject (state, payload) {
    state.langObject = payload;
  }
};

export const actions = {
  getLangList ({ commit, state }, func) {
    getLangList().then(({ data }) => {
      const langList = data;
      const langObject = {};
      for (const i in data) {
        const item = data[i];
        langObject[item.lang] = item;
      }
      commit('setLangList', langList);
      commit('setLangObject', langObject);
      func && func(data);
    });
  }
};
