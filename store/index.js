import { getField, updateField } from 'vuex-map-fields';
import { getProfile } from '~/api/user.js';

export const state = () => ({
  userInfo: null,
  collapseSwitch: false,
  confirmMessage: '',
  confirmOk: null,
  confirmCancel: null,
  alertMessage: '',
  ad: null
});

export const mutations = {
  updateField,
  setData (state, payload) {
    state[payload.item] = payload.data;
  },
  setWindowsData (state, payload) {
    state.confirmMessage = payload.message;
    state.confirmOk = payload.ok;
    state.confirmCancel = payload.cancel;
  },
  resetData (state, payload) {
    state[payload.item] = null;
  }
};

export const actions = {
  async setUserInfo ({ commit, dispatch, state }, callback) {
    try {
      const res = await getProfile();
      res.data.amount = (res.data.amount * 1).toFixed(2);
      commit('setData', { item: 'userInfo', data: res.data });
      callback && callback(res);
    } catch (err) {
      dispatch('clearUserInfo');
      callback && callback(err);
    }
  },
  setConfirmMessage ({ commit }, data) {
    commit('setWindowsData', data);
  },
  setAlertMessage ({ commit }, val) {
    commit('setData', { item: 'alertMessage', data: val });
  },
  setAd ({ commit }, val) {
    commit('setData', { item: 'ad', data: val });
  },
  clearUserInfo ({ commit, state }, callback) {
    commit('resetData', { item: 'userInfo' });
    callback && callback();
  }
};

export const getters = {
  getField
};
