import { fetcher } from '~/assets/scripts/fetcher.js';

export const login = async (data) => {
  const result = await fetcher('/login', 'POST', {}, data);
  return result;
};

export const logout = async () => {
  const result = await fetcher('/logout', 'GET');
  return result;
};

// num:0(登入),1(註冊),2(忘記密碼)
export const getCaptcha = async (num) => {
  const result = await fetcher(`/captcha/${num}`, 'GET');
  return result;
};

export const getProfile = async () => {
  const result = await fetcher('/profile', 'GET');
  return result;
};

export const getNewsList = async (lang) => {
  const result = await fetcher('/getNews', 'GET', { lang });
  return result;
};

export const getAdsList = async () => {
  const result = await fetcher('/getAd', 'GET');
  return result;
};

export const sendPassword = async (data) => {
  const result = await fetcher('/forget', 'POST', {}, data);
  return result;
};

export const sendVerificationCode = async (data) => {
  const result = await fetcher('/sendSMS', 'POST', {}, data);
  return result;
};

export const register = async (data) => {
  const result = await fetcher('/register', 'POST', {}, data);
  return result;
};

export const activeAccount = async (data) => {
  const result = await fetcher('/activeAccount', 'POST', {}, data);
  return result;
};
