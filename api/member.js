import { fetcher } from '~/assets/scripts/fetcher.js';

export const getMemberInfo = async (id) => {
  const result = await fetcher(`/member/member-listing?where[id]=${id}`, 'GET');
  return result;
};

export const updateMemberInfo = async (data, id) => {
  const result = await fetcher(`/member/member-listing/${id}`, 'PUT', {}, data);
  return result;
};
