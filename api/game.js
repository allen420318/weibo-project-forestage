import { fetcher } from '~/assets/scripts/fetcher.js';

export const getBalance = async (data) => {
  const result = await fetcher('/game/GetBalance', 'POST', data);
  return result;
};

export const openGame = async (data) => {
  const result = await fetcher('/game/openGame', 'POST', data);
  return result;
};

export const AtoBpoint = async (data) => {
  const result = await fetcher('/game/AtoBpoint', 'POST', data);
  return result;
};

export const getProductList = async (params) => {
  const result = await fetcher('product/product-main', 'GET', params);
  return result;
};

export const getGameList = async (params) => {
  const result = await fetcher('product/game-type', 'GET', params);
  return result;
};
