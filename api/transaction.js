import { fetcher } from '~/assets/scripts/fetcher.js';

export const getPaymentList = async () => {
  const result = await fetcher('/getPayTypeList', 'GET');
  return result;
};

export const getPromoList = async () => {
  const result = await fetcher('/getPromoList', 'GET');
  return result;
};

export const deposit = async (data) => {
  const result = await fetcher('/generateOrder', 'POST', {}, data);
  return result;
};

export const getRecordList = async (params) => {
  const result = await fetcher('/order', 'GET', params);
  return result;
};

export const withdrawal = async (data) => {
  const result = await fetcher('/withdrawal', 'POST', {}, data);
  return result;
};

export const getBettingRecordList = async (params) => {
  const result = await fetcher('report/report', 'GET', params);
  return result;
};

export const virtualDeposit = async (params) => {
  const result = await fetcher('fxbit', 'GET', params);
  return result;
};

export const virtualWithdrawal = async (params) => {
  const result = await fetcher('fxbit/outPay', 'GET', params);
  return result;
};

export const virtualDepositDetail = async (params) => {
  const result = await fetcher('fxbit/getPayData', 'GET', params);
  return result;
};

// export const getExchangeRate = async (params) => {
//   const result = await fetcher('/getOnlyRate', 'GET', params);
//   return result;
// };

export const getVirtualBuyingRate = async (params) => {
  const result = await fetcher('/fxbit', 'GET', params);
  return result;
};

export const getVirtualSellingRate = async (params) => {
  const result = await fetcher('/fxbit/outPay', 'GET', params);
  return result;
};

export const transABLogList = async (data) => {
  const result = await fetcher('/transABLog', 'GET', data);
  return result;
};

export const getBankList = async (data) => {
  const result = await fetcher('/bank/banks', 'GET', data);
  return result;
};

export const getBankAccount = async (data) => {
  const result = await fetcher('/bank/bank-accounts', 'GET', data);
  return result;
};

export const getPayRate = async (data) => {
  const result = await fetcher('/getPayRate', 'GET', data);
  return result;
};
