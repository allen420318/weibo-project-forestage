import { fetcher } from '~/assets/scripts/fetcher.js';

export const get = async (params) => {
  const result = await fetcher('inbox', 'GET', params);
  return result;
};

// export const add = async (data) => {
//   const result = await fetcher('inbox', 'POST', {}, data);
//   return result;
// };

export const update = async (data, id) => {
  const result = await fetcher(`inbox/${id}`, 'PUT', {}, data);
  return result;
};

export const del = async (id) => {
  const result = await fetcher(`inbox/${id}`, 'DELETE');
  return result;
};
