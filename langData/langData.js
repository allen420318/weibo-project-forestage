export default
{
  微博帝國: {
    en: 'Weibo Empire',
    th: 'Weibo Empire',
    ja: 'Weibo Empire'
  },
  移動端: {
    en: 'Mobile',
    th: 'มือถือ',
    ja: 'モバイル'
  },
  歡迎: {
    en: 'Welcome',
    th: 'ยินดีต้อนรับ',
    ja: 'ようこそ'
  },
  註冊: {
    en: 'Register',
    th: 'สมัครสมาชิก',
    ja: '登録'
  },
  用戶名: {
    en: 'Username',
    th: 'ชื่อผูู้ใช้',
    ja: 'ユーザー名'
  },
  密碼: {
    en: 'Password',
    th: 'รหัสผ่าน',
    ja: 'パスワード'
  },
  代碼: {
    en: 'Code',
    th: 'รหัส',
    ja: 'コード'
  },
  忘記密碼: {
    en: 'Forgot Password',
    th: 'ลืมรหัสผ่าน',
    ja: 'パスワードをお忘れました'
  },
  激活帳戶: {
    en: 'Account Activation',
    th: 'การเปิดใช้งานบัญชี',
    ja: 'アカウントアクティベーション'
  },
  登入: {
    en: 'Login',
    th: 'เข้าสู่ระบบ',
    ja: 'ログイン'
  },
  會員登入: {
    en: 'Login',
    th: 'เข้าสู่ระบบ',
    ja: 'ログイン'
  },
  登出: {
    en: 'Logout',
    th: 'ออกจากระบบ',
    ja: 'ログアウト'
  },
  錢包: {
    en: 'Cashier',
    th: 'แคชเชียร์',
    ja: 'ウォレット'
  },
  總餘額: {
    en: 'Total Balance',
    th: 'ยอดรวม',
    ja: '残高の合計'
  },
  我的帳號: {
    en: 'My Account',
    th: 'บัญชีของฉัน',
    ja: 'アカウント'
  },
  交易紀錄: {
    en: 'History',
    th: 'ประวัติ',
    ja: '履歴'
  },
  在線客服: {
    en: 'Contact CS',
    th: 'ติดต่อ CS',
    ja: 'サービス'
  },
  最新消息: {
    en: 'News',
    th: 'ข่าว',
    ja: 'ニュース'
  },
  關閉: {
    en: 'Close',
    th: 'ปิด',
    ja: '閉じる'
  },
  Android: {
    en: 'Android',
    th: 'Android',
    ja: 'アンドロイド'
  },
  關於我們: {
    en: 'About Us Term',
    th: 'เกี่ยวกับเรา',
    ja: '私たちに関して'
  },
  規則條款: {
    en: 'Term & Condition',
    th: 'ข้อตกลงและเงื่อนไข',
    ja: '利用規約'
  },
  博彩責任: {
    en: 'Responsible Gaming',
    th: 'ความรับผิดชอบในการเล่นเกมส์',
    ja: '責任あるゲーム'
  },
  隱私政策: {
    en: 'Privacy Policy',
    th: 'นโยบายส่วนบุคคล',
    ja: 'プライバシーポリシー'
  },
  銀行: {
    en: 'Bank',
    th: 'ธนาคาร',
    ja: '支払いオプション'
  },
  聯繫我們: {
    en: 'Contact Us',
    th: 'ติดต่อเรา',
    ja: 'お問い合わせ'
  },
  合作廠商: {
    en: 'Partners',
    th: 'พันธมิตร',
    ja: 'アフェリエイト'
  },
  合營夥伴: {
    en: 'Partners',
    th: 'พันธมิตร',
    ja: 'アフェリエイト'
  },
  Copyright: {
    en: 'Copyright',
    th: 'Copyright',
    ja: '著作権'
  },
  主頁: {
    en: 'Home',
    th: 'หน้าหลัก',
    ja: 'ホーム'
  },
  體育: {
    en: 'Sportsbook',
    th: 'กีฬา',
    ja: 'スポーツベット'
  },
  真人娛樂: {
    en: 'Live Casino',
    th: 'คาสิโนสด',
    ja: 'ライブカジノ'
  },
  老虎機: {
    en: 'Slots',
    th: 'สล็อต',
    ja: 'スロット'
  },
  捕魚: {
    en: 'Fish Hunter',
    th: 'เกมส์ยิงปลา',
    ja: 'フィッシュハンター'
  },
  六合彩: {
    en: 'Lottery',
    th: 'หวย',
    ja: '宝くじ'
  },
  彩票: {
    en: 'Lottery',
    th: 'หวย',
    ja: '宝くじ'
  },
  賽馬: {
    en: 'Horse Racng',
    th: 'การแข่งม้า',
    ja: '競馬'
  },
  電競: {
    en: 'Esport',
    th: 'อีสปอร์ต',
    ja: 'Eスポーツ'
  },
  棋牌: {
    en: 'Card Game',
    th: 'เกมการ์ด',
    ja: 'ポーカー'
  },
  優惠活動: {
    en: 'Promotion',
    th: 'โปรโมชั่น',
    ja: 'プロモーション'
  },
  '手機 APP 下載': {
    en: 'Mobile App',
    th: 'แอพมือถือ',
    ja: 'モバイルアプリ'
  },
  '電影 / 音樂分享': {
    en: 'Movie / Music Sharing',
    th: 'ภาพยนตร์ / การแบ่งปันเพลง',
    ja: '映画 / 音楽の共有'
  },
  '立即下載手機 APP': {
    en: 'Download The App Now',
    th: 'ดาวน์โหลดแอปทันที',
    ja: '今すぐアプリをダウンロード'
  },
  '手機快速投注 現金輕鬆入袋': {
    en: 'Mobile fast bet, easy cash in pocket',
    th: 'เดิมพันที่รวดเร็วบนมือถือเงินสดในกระเป๋าง่ายๆ',
    ja: '携帯電話ですぐ賭けて、早く簡単にお金が手元に'
  },
  以手機實現方便簡單的操作方式: {
    en: 'Its easier and convenient to play on Mobile',
    th: 'ง่ายและสะดวกในการเล่นบนมือถือ',
    ja: '携帯電話で簡単に便利に操作'
  },
  '彩票投注、真人視訊、電子遊藝、體育投注 四大平台': {
    en: 'Lottery , live bet, Slot, Sports bet. 4 major platform',
    th: 'ลอตเตอรี, เดิมพันสด, สล็อต, เดิมพันกีฬา 4 แพลตฟอร์มหลัก',
    ja: '宝くじ、ライブカジノ、スロットマシーン、スポーツベット、４つの大きなプラットフォーム'
  },
  '安全有保障 服務新趨勢': {
    en: 'High security , new way of service',
    th: 'ความปลอดภัยสูงวิธีใหม่ในการให้บริการ',
    ja: '安心安全のセキュリティ、新しいサービス'
  },
  '登入密碼保障，充值、提現、迅速到帳': {
    en: 'Secure Longin password, Fast recharge and withdraw',
    th: 'รหัสผ่านเข้าสู่ระบบที่ปลอดภัยเติมเงินและถอนได้อย่างรวดเร็ว',
    ja: 'ログインパスワードの保護、迅速な出入金'
  },
  為您打造最安全的服務平台: {
    en: 'The safest service platform for you',
    th: 'แพลตฟอร์มบริการที่ปลอดภัยที่สุดสำหรับคุณ',
    ja: '最も安全なプラットフォーム'
  },
  '讓您暢玩無憂、領獎無慮': {
    en: 'play and win without any concern',
    th: 'เล่นและชนะโดยไม่ต้องกังวลใดๆ',
    ja: '気楽にプレイして賞金獲得'
  },
  帳戶: {
    en: 'Profile',
    th: 'ข้อมูลส่วนตัว',
    ja: 'プロフィール'
  },
  提款: {
    en: 'Withdrawal',
    th: 'ถอนเงิน',
    ja: '引き出し'
  },
  轉帳: {
    en: 'Transfer',
    th: 'โยกเงิน',
    ja: '転送'
  },
  存款: {
    en: 'Deposit',
    th: 'ฝากเงิน',
    ja: '預金'
  },
  電郵: {
    en: 'Email',
    th: 'อีเมล',
    ja: 'メール'
  },
  聯繫號碼: {
    en: 'Contact No.',
    th: 'เบอร์โทรศัพท์',
    ja: '電話番号'
  },
  全名: {
    en: 'Full Name',
    th: 'ชื่อ-นามสกุล',
    ja: '氏名'
  },
  更改密碼: {
    en: 'Change Password',
    th: 'เปลี่ยนรหัสผ่าน',
    ja: 'パスワードを変更する'
  },
  當前密碼: {
    en: 'Current Password',
    th: 'รหัสผ่านปัจจุบัน',
    ja: '現在のパスワード'
  },
  新密碼: {
    en: 'New Password',
    th: 'รหัสผ่านใหม่',
    ja: '新しいパスワード'
  },
  確認新密碼: {
    en: 'Confirm Password',
    th: 'ยืนยันรหัสผ่าน',
    ja: 'パスワードを認証する'
  },
  個人資料: {
    en: 'Personal Detail',
    th: 'รายละเอียดส่วนบุคคล',
    ja: '個人の詳細'
  },
  出生日期: {
    en: 'Birth Date',
    th: 'วันเดือนปีเกิด',
    ja: '誕生日'
  },
  性別: {
    en: 'Gender',
    th: 'เพศ',
    ja: '性別'
  },
  男: {
    en: 'Male',
    th: 'ชาย',
    ja: '男性'
  },
  女: {
    en: 'Female',
    th: 'หญิง',
    ja: '女性'
  },
  提交: {
    en: 'Submit',
    th: 'ส่ง',
    ja: '提出'
  },
  報表類型: {
    en: 'Report Type',
    th: 'ประเภทรายการ',
    ja: 'レポートのタイプ'
  },
  請選擇: {
    en: 'Please Select',
    th: 'กรุณาเลือก',
    ja: '選んでください'
  },
  下注: {
    en: 'Bet',
    th: 'เดิมพัน',
    ja: 'ベット'
  },
  紅利優惠: {
    en: 'Promotion',
    th: 'โปรโมชั่น',
    ja: 'プロモーション'
  },
  由: {
    en: 'From',
    th: 'จาก',
    ja: 'から'
  },
  至: {
    en: 'To',
    th: 'ถึง',
    ja: 'へ'
  },
  '請輸入有效或正確日期時間規格，例如：2018-01-01.': {
    en: 'Please insert valid datetime format, e.g. : 2018-01-01.',
    th: 'โปรดใส่รูปแบบวันที่และเวลาที่ถูกต้องเช่น : 2018-01-01',
    ja: '有効な日時形式を入力してください。 例：2018-01-01。'
  },
  顯示筆數: {
    en: 'Show',
    th: '',
    ja: ''
  },
  產品: {
    en: 'Product Name',
    th: 'ชื่อสินค้า',
    ja: '製品名'
  },
  總記錄條數: {
    en: 'Total Records',
    th: 'จำนวนรวมทั้งหมด',
    ja: '記録の合計'
  },
  總投注: {
    en: 'Total Bet',
    th: 'เดิมพันทั้งหมด',
    ja: 'ベット合計'
  },
  總賠付: {
    en: 'Total Payout',
    th: 'การจ่ายเงินทั้งหมด',
    ja: '支払い合計'
  },
  總輸贏: {
    en: 'Total Winlose',
    th: 'จำนวน ชนะ-แพ้',
    ja: '勝ち負けの合計'
  },
  没有匹配结果: {
    en: 'No match',
    th: 'ไม่ตรงกัน',
    ja: '一致する結果がありません'
  },
  交易時間: {
    en: 'Transaction Time',
    th: 'เวลาทำรายการ',
    ja: '取引時間'
  },
  交易編碼: {
    en: 'Transaction No',
    th: 'หมายเลขธุรกรรม',
    ja: '取引番号'
  },
  總額: {
    en: 'Amount',
    th: 'จำนวนเงิน',
    ja: '金額'
  },
  存款方式: {
    en: 'Deposit Method',
    th: 'วิธีการฝากเงิน',
    ja: '入金方法'
  },
  點數: {
    en: 'Points',
    th: 'คะแนน',
    ja: 'ポイント'
  },
  錢包地址: {
    en: 'Wallet address',
    th: 'ที่อยู่กระเป๋าเงิน',
    ja: 'ウォレットアドレス'
  },
  狀態: {
    en: 'Statu',
    th: 'สถานะ',
    ja: '状態'
  },
  '顯示第 0 至 0 項結果，共 0 項': {
    en: 'Page 0 to Page0, total 0',
    th: 'หน้า 0 ถึงหน้า 0 รวม 0',
    ja: '０項目から０項目の中で０の結果を表示します'
  },
  上頁: {
    en: 'Previous',
    th: 'ก่อนหน้า',
    ja: '前'
  },
  下頁: {
    en: 'Next',
    th: 'ต่อไป',
    ja: '次'
  },
  備註: {
    en: 'Remark',
    th: 'คำพูด',
    ja: '備考'
  },
  流水要求: {
    en: 'Turnover Requirement',
    th: 'ความต้องการยอดเดิมพันหมุนเวียน',
    ja: 'ターンオーバー要件'
  },
  帳戶餘額: {
    en: 'Balance',
    th: 'ยอดเงินคงเหลือ',
    ja: '残高'
  },
  每天只可有3次提款: {
    en: 'Can only withdraw 3 times per day',
    th: 'ถอนได้เพียง 3 ครั้งต่อวัน',
    ja: '1日3回まで出金できます'
  },
  取款方法: {
    en: 'Withdrawal Method',
    th: 'วิธีการถอน',
    ja: '引き出し方法'
  },
  支付寶轉支付寶: {
    en: 'From Alipay to Alipay',
    th: 'จาก Alipay ถึง Alipay',
    ja: 'アリペイからアリペイへ'
  },
  轉數快: {
    en: 'Faster Payment System',
    th: 'ระบบการชำระเงินที่เร็วขึ้น',
    ja: 'QRコード支払'
  },
  銀行轉帳: {
    en: 'Bank Transfer',
    th: 'โอนเงินผ่านธนาคาร',
    ja: '銀行振込'
  },
  銀行名稱: {
    en: 'Bank Name',
    th: 'ชื่อธนาคาร',
    ja: '銀行名'
  },
  分行代號: {
    en: 'Branch code',
    th: 'รหัสสาขา',
    ja: '支店コード'
  },
  帳戶號碼: {
    en: 'Bank Account Number',
    th: 'หมายเลขบัญชีธนาคาร',
    ja: '銀行の口座番号'
  },
  主錢包餘額: {
    en: 'Main Wallet',
    th: 'กระเป๋าสตางค์หลัก',
    ja: 'メインウォレット'
  },
  兌換成娛樂城: {
    en: 'Exchange into a casino',
    th: 'แลกเปลี่ยนเป็นคาสิโน',
    ja: 'カジノへの交換'
  },
  參與優惠紅利: {
    en: 'Participate in discount bonus',
    th: 'เข้าร่วมในโบนัสส่วนลด',
    ja: '割引ボーナスに参加する'
  },
  點數約: {
    en: 'Approximate points',
    th: 'คะแนนโดยประมาณ',
    ja: 'おおよそのポイント'
  },
  兌換成: {
    en: 'Convert to',
    th: 'เปลี่ยนเป็น',
    ja: 'に変換'
  },
  約: {
    en: 'approximately',
    th: 'ประมาณ',
    ja: '約'
  },
  '請輸入轉讓總額，最高轉帳額為$49999': {
    en: 'Please enter the total transfer amount, the maximum transfer amount is $49999',
    th: 'กกรุณากรอกจำนวนเงินที่โอนทั้งหมดจำนวนเงินที่โอนสูงสุดคือ $ 49999',
    ja: '合計送金金額を入力してください。最大送金金額は$ 49999です。'
  },
  全轉至主錢包: {
    en: 'Transfer All',
    th: 'ถอนทั้งหมด',
    ja: 'すべてを送金'
  },
  存款渠道: {
    en: 'Topup way',
    th: 'วิธีเติมเงิน',
    ja: '入金の仕方'
  },
  存款選項: {
    en: 'Deposit Method',
    th: 'วิธีการฝากเงิน',
    ja: '入金方法'
  },
  優惠紅利: {
    en: 'Bonus',
    th: 'โบนัส',
    ja: 'ボーナス'
  },
  支付通道: {
    en: 'Payment channel',
    th: 'ช่องทางการชำระเงิน',
    ja: '支払いチャネル'
  },
  最高存款: {
    en: 'Maximum Deposit',
    th: 'เงินฝากสูงสุด',
    ja: '最大預金'
  },
  最低存款: {
    en: 'Minimum Deposit',
    th: 'เงินฝากขั้นต่ำ',
    ja: '最低入金額'
  },
  '系統強制給小數後兩位 : $0': {
    en: 'System will Automatically give out decimal point : $0',
    th: 'ระบบจะให้จุดทศนิยมโดยอัตโนมัติ : $0',
    ja: 'システムが自動的の小数点$0まで表します。'
  },
  '請於下單後五分鐘內付款.': {
    en: 'Please make the payment within 5 minute after you request .',
    th: 'กรุณาชำระเงินภายใน 5 นาทีหลังจากที่คุณร้องขอ',
    ja: '入金リクエストをしてから5分以内にお支払いください'
  },
  '逾時請重新提交訂單，不要超時後付款': {
    en: 'Please fill in the account number or phone number of your customer in the message area of Alipay when transferring funds.',
    th: 'โปรดทำการร้องขอการชำระเงินใหม่หากเซสชันล่วงเวลาโปรดอย่าชำระเงินใด ๆ หากล่วงเวลา',
    ja: 'Alipayの転送のメッセージ領域に顧客のアカウント番号または電話番号を入力してください'
  },
  '請於支付寶轉帳時的訊息處，填寫貴客戶的帳號或電話號碼': {
    en: 'Please make a new payment request if session overtime, please do not make any payment if its overtime.',
    th: 'รุณากรอกหมายเลขบัญชีหรือหมายเลขโทรศัพท์ของลูกค้าของคุณในพื้นที่ข้อความของ Alipay เมื่อโอนเงิน',
    ja: 'タイムアウトした場合はお支払いを行わず、再度リクエストしてください'
  },
  '選取紅利，有效投注要求10倍流水才能提款。': {
    en: 'If you select Bonus, you will need to have 10 times rollover before you can withdraw',
    th: 'หากคุณเลือกโบนัสคุณจะต้องมียอดโรลโอเวอร์ 10 เท่าก่อนจึงจะสามารถถอนได้',
    ja: '入金時にボーナスを獲得した場合、出金条件額が10倍になります'
  },
  '請輸入您的手機號碼(限數字)，必須驗證帳戶才開通': {
    en: 'Please provide a valid contact number (numbers only) for future promotion and payment correspondences.',
    th: 'โปรดระบุหมายเลขเบอร์โทรศัพท์ที่ถูกต้อง (ตัวเลขเท่านั้น) สำหรับโปรโมชั่นอื่นๆ และการชำระเงิน',
    ja: '今後のプロモーションおよび支払い対応のために、有効な連絡先番号（番号のみ）を入力してください。'
  },
  激活碼: {
    en: 'Activation Code',
    th: 'รหัสเปิดใช้งาน',
    ja: 'アクティベーションコード'
  },
  驗證碼: {
    en: 'Verification Code',
    th: 'รหัสยืนยัน',
    ja: '検証コード'
  },
  重新發送激活碼: {
    en: 'Resend Code',
    th: 'ส่งรหัสอีกครั้ง',
    ja: 'コードを再送信'
  },
  激活: {
    en: 'Activation',
    th: 'การกระตุ้น',
    ja: 'アクティベーション'
  },
  重輸密碼: {
    en: 'Confirm Password',
    th: 'ยืนยันรหัสผ่าน',
    ja: 'パスワードを認証する'
  },
  '請輸入能收到SMS驗證碼之電話號碼，否則不能註冊': {
    en: 'Please enter a phone number that can receive the SMS verification code, otherwise you cannot register',
    th: 'โปรดป้อนหมายเลขโทรศัพท์ที่สามารถรับรหัสยืนยันทาง SMS ไม่เช่นนั้นคุณจะไม่สามารถลงทะเบียนได้',
    ja: 'SMS認証コードを受信できる電話番号を入力してください。入力しないと登録できません。'
  },
  '輸入6至12位的數字或小寫英文字母組合(a-z,0-9)': {
    en: 'Enter 6-12 digits or a combination of lowercase English letters (a-z, 0-9)',
    th: 'ป้อนตัวเลข 6-12 หลักหรือตัวอักษรภาษาอังกฤษตัวพิมพ์เล็กผสมกัน (a-z, 0-9)',
    ja: '6〜12桁、または小文字の英字（a〜z、0〜9）の組み合わせを入力します'
  },
  '輸入6至12位含英文與數字的密碼組合，不允許特殊符號': {
    en: 'Between 6 to 12 character',
    th: 'ระหว่าง6ถึง12ตัวอักษร',
    ja: ''
  },
  再次輸入密碼: {
    en: 'Confirm Password',
    th: 'ยืนยันรหัสผ่าน',
    ja: 'パスワードの再入力'
  },
  '輸入能收到SMS驗證碼之電話號碼，否則不能註冊': {
    en: 'Please enter a phone number that can receive the SMS verification code, otherwise you cannot register',
    th: 'โปรดป้อนหมายเลขโทรศัพท์ที่สามารถรับรหัสยืนยันทาง SMS ไม่เช่นนั้นคุณจะไม่สามารถลงทะเบียนได้',
    ja: 'SMS確認コードを受信できる電話番号を入力してください。入力しないと登録できません。'
  },
  請輸入用戶姓名: {
    en: 'Please insert Username.',
    th: 'กรุณาใส่ชื่อผู้ใช้',
    ja: 'ユーザー名を入力してください。'
  },
  請輸入密碼: {
    en: 'Please insert password.',
    th: 'กรุณาใส่รหัสผ่าน',
    ja: 'パスワードを入力してください。'
  },
  驗證碼無效: {
    en: 'Invalid verification code',
    th: 'รหัสยืนยันไม่ถูกต้อง',
    ja: '無効な検証コード'
  },
  請輸入手機驗證碼: {
    en: 'Phone verification code',
    th: 'รหัสยืนยันทางโทรศัพท์',
    ja: '携帯電話検証コード'
  },
  手機驗證碼: {
    en: 'Phone verification code',
    th: 'รหัสยืนยันทางโทรศัพท์',
    ja: '携帯電話検証コード'
  },
  圖形驗證碼: {
    en: 'Graphic verification code',
    th: 'รหัสยืนยันกราฟิก',
    ja: '画像認証コード'
  },
  會員註冊: {
    en: 'Register',
    th: 'สมัครสมาชิก',
    ja: '登録'
  },
  發送驗證碼: {
    en: 'Send Verification Code',
    th: 'ส่งรหัสยืนยัน',
    ja: '検証コードを送信'
  },
  創建一個賬戶: {
    en: 'Create account',
    th: 'สร้างบัญชี',
    ja: 'アカウントを作成する'
  },
  '需要幫忙？ 通過以下渠道聯繫我們的客戶服務人員': {
    en: 'Need Help? Contact our customer service via the following channels',
    th: 'ต้องการความช่วยเหลือ? ติดต่อฝ่ายบริการลูกค้าของเราได้ตามช่องทางต่อไปนี้',
    ja: 'ヘルプが必要ですか？カスタマーサービスにお問い合わせください。'
  },
  Wechat: {
    en: 'Wechat',
    th: 'Wechat',
    ja: 'ウィーチャット'
  },
  Whatsapp: {
    en: 'Whatsapp',
    th: 'Whatsapp',
    ja: 'ワッツアップ'
  },
  Telegram: {
    en: 'Telegram',
    th: 'โทรเลข',
    ja: 'テレグラム'
  },
  Skype: {
    en: 'Skype',
    th: 'Skype',
    ja: 'スカイプ'
  },
  Line: {
    en: 'Line',
    th: 'ไลน์',
    ja: 'ライン'
  },
  'QR Code': {
    en: 'QR Code',
    th: 'คิวอาร์โค้ด',
    ja: 'QRコード'
  },
  Yahoo: {
    en: 'Yahoo',
    th: 'Yahoo',
    ja: 'ヤフー'
  },
  商戶門店入現金: {
    en: 'Store Cash',
    th: 'เก็บเงินสด',
    ja: '商店の現金'
  },
  '銀行轉帳/如鈔機': {
    en: 'Bank transfer / Money Machine, etc.',
    th: 'โอนเงินผ่านธนาคาร / เครื่องรับเงินฯลฯ',
    ja: '銀行振込/マネーマシン等'
  },
  全部: {
    en: 'All',
    th: 'ทั้งหมด',
    ja: 'すべて'
  },
  找不到頁面: {
    en: 'Sorry! Page not found',
    th: 'ขออภัย! ไม่พบหน้านี้',
    ja: '大変申し訳ありませんが、ページが見つかりません'
  },
  'Play Now': {
    en: 'Play Now',
    th: 'เล่นเลย',
    ja: '今すぐプレイ'
  },
  取消: {
    en: 'Cancel',
    th: 'ยกเลิก',
    ja: 'キャンセル'
  },
  API不支援此方法呼叫: {
    en: 'API Error',
    th: 'ข้อผิดพลาดของ API',
    ja: 'APIエラー'
  },
  資料寫入失敗: {
    en: 'Fail to fill up the form',
    th: 'กรอกแบบฟอร์มไม่สำเร็จ',
    ja: 'データの書き込みに失敗しました'
  },
  會員資料不存在: {
    en: 'Invalid member ID',
    th: 'รหัสสมาชิกไม่ถูกต้อง',
    ja: '無効なメンバーID'
  },
  會員帳號已註冊: {
    en: 'Member already excited',
    th: 'สมาชิกตื่นเต้นแล้ว',
    ja: 'このユーザー名は既に使われています。'
  },
  會員手機已註冊: {
    en: 'Member Mobile number already excited',
    th: 'สมาชิกหมายเลขโทรศัพท์มือถือตื่นเต้นแล้ว',
    ja: 'この電話番号は既に使われています。'
  },
  會員帳號格式錯誤: {
    en: 'Invilid member account format entey',
    th: 'รายการรูปแบบบัญชีสมาชิกไม่ถูกต้อง',
    ja: 'アカウント名の書式が正しくありません'
  },
  會員手機格式錯誤: {
    en: 'Invilid mobile number format entry',
    th: 'รายการรูปแบบหมายเลขโทรศัพท์มือถือไม่ถูกต้อง',
    ja: '電話番号の書式が正しくありません。'
  },
  密碼格式錯誤: {
    en: 'Invilid password format entry',
    th: 'รายการรูปแบบรหัสผ่านไม่ถูกต้อง',
    ja: 'パスワードの書式が正しくありません'
  },
  驗證碼輸入錯誤: {
    en: 'Incorrect verification code',
    th: 'รหัสยืนยันไม่ถูกต้อง',
    ja: '間違った確認コード'
  },
  驗證手機與輸入手機不同: {
    en: 'Phone number does not match.',
    th: 'หมายเลขโทรศัพท์ไม่ตรงกัน',
    ja: '電話番号が一致しません'
  },
  密碼與確認密碼不同: {
    en: 'The password confirmation does not match.',
    th: 'การยืนยันรหัสผ่านไม่ตรงกัน',
    ja: 'パスワードの確認が一致しません。'
  },
  簡訊驗證碼輸入錯誤: {
    en: 'Invalid SMS verification code',
    th: 'รหัสยืนยันทาง SMS ไม่ถูกต้อง',
    ja: '無効なSMS確認コード'
  },
  請先登入會員: {
    en: 'Please login first',
    th: 'กรุณาเข้าสู่ระบบก่อน',
    ja: 'ログインしてください'
  },
  帳號尚未啟用: {
    en: 'Account has not been activated.',
    th: 'ยังไม่ได้เปิดใช้งานบัญชี',
    ja: 'アカウントが有効になっていません'
  },
  帳號已停權: {
    en: 'Account suspended',
    th: 'บัญชีที่ถูกระงับ',
    ja: 'アカウントは停止されました'
  },
  登入異常: {
    en: 'Login Error',
    th: 'ข้อผิดพลาดในการเข้าสู่ระบบ',
    ja: 'ログインエラー'
  },
  帳號或密碼錯誤: {
    en: 'Invalid account ID or password',
    th: 'บัญชีหรือรหัสผ่านไม่ถูกต้อง',
    ja: 'アカウント ID またはパスワードが無効です'
  },
  會員已在其他地方登入: {
    en: 'Already logged in elsewhere',
    th: 'เข้าสู่ระบบที่อื่นแล้ว',
    ja: '他の場所でログインされています。'
  },
  您的IP已遭拒絕登入: {
    en: 'Your IP address is denied access.',
    th: 'ที่อยู่ IP ของคุณถูกปฏิเสธการเข้าถึง',
    ja: 'IPアドレスが拒否されました'
  },
  帳號已刪除: {
    en: 'Account has been deleted',
    th: 'ลบบัญชีแล้ว',
    ja: 'アカウントが削除されました'
  },
  帳號或密碼為空: {
    en: 'Invalid account or password. The account or password is empty.',
    th: 'บัญชีหรือรหัสผ่านว่างเปล่า',
    ja: 'アカウントまたはパスワードが空です。'
  },
  密碼重設簡訊發送失敗: {
    en: 'Fail to send SMS verifycation code',
    th: 'ส่งรหัสยืนยันทาง SMS ไม่สำเร็จ',
    ja: 'SMS認証コードの送信に失敗しました'
  },
  '每次最高轉帳額為$49999，如金額超過，請分開多次轉帳': {
    en: 'The maximum transfer amount is $ 49999 at a time. If the transfer amount exceeds the upper limit, please transfer in multiple times.',
    th: 'จำนวนเงินโอนสูงสุดคือ $ 49999 ต่อครั้ง หากยอดการโอนเกินขีด จำกัด ด้านบนกรุณาโอนหลายครั้ง',
    ja: '振り込み金額の上限は一回に付き最大$49999です。振り込み金額が上限を超える場合は複数回に分けて振り込みをお願いします。'
  },
  重新下單: {
    en: 'Please apply for the order again.',
    th: 'กรุณาสมัครคำสั่งซื้ออีกครั้ง',
    ja: 'もう一度注文を申請してください'
  },
  Loading: {
    en: 'Loading',
    th: 'กำลังโหลด',
    ja: '読み込んでいます'
  },
  快速轉帳: {
    en: 'Fast transfer',
    th: 'โอนเร็ว',
    ja: '高速転送'
  },
  目前的遊戲餘額: {
    en: 'Current game balance',
    th: 'ความสมดุลของเกมในปัจจุบัน',
    ja: '現在のゲームバランス'
  },
  轉帳金額: {
    en: 'Transfer amount',
    th: 'จำนวนเงินที่โอน',
    ja: '払込金額'
  },
  '僅能輸入數字，且不能低於0.01': {
    en: 'Only numbers can be entered and cannot be lower than 0.01',
    th: 'ป้อนได้เฉพาะตัวเลขและต้องไม่ต่ำกว่า 0.01',
    ja: '入力できるのは数字のみで、0.01以上にする必要があります'
  },
  忽略並啟動遊戲: {
    en: 'Ignore and start the game',
    th: 'ละเว้นและเริ่มเกม',
    ja: '無視してゲームを開始します'
  },
  請先登入: {
    en: 'Please login first',
    th: 'กรุณาเข้าสู่ระบบก่อน',
    ja: '最初にログインしてください'
  },
  提示訊息: {
    en: 'Prompt message',
    th: 'ข้อความแจ้ง',
    ja: 'プロンプトメッセージ'
  },
  請輸入聯繫號碼: {
    en: 'Please enter the contact number',
    th: 'กรุณากรอกหมายเลขติดต่อ',
    ja: '連絡先を入力してください'
  },
  聯繫號碼有問題: {
    en: 'Problem with contact number',
    th: 'ปัญหาเกี่ยวกับหมายเลขติดต่อ',
    ja: '連絡先番号の問題'
  },
  圖形驗證碼為4碼組合: {
    en: 'Graphic verification code is a combination of 4 codes',
    th: 'รหัสยืนยันกราฟิกคือการรวมกันของรหัส 4 ตัว',
    ja: 'グラフィック検証コードは4つのコードの組み合わせです'
  },
  數字或英文組合4到10碼: {
    en: 'Number or English combination 4 to 10 codes',
    th: 'วเลขหรือรหัสผสมภาษาอังกฤษ 4 ถึง 10 รหัส',
    ja: '数字または英語の組み合わせ4〜10コード'
  },
  連續英文數字組合6到13碼: {
    en: '6 to 13 digits of consecutive English numbers',
    th: 'ตัวเลขภาษาอังกฤษติดต่อกัน 6 ถึง 13 หลัก',
    ja: '6〜13桁の連続した英語番号'
  },
  必須跟密碼一樣: {
    en: 'Must be the same as the password',
    th: 'ต้องเหมือนกับรหัสผ่าน',
    ja: 'パスワードと同じである必要があります'
  },
  此欄位不能空白: {
    en: 'This field cannot be blank',
    th: 'ลด์นี้ต้องไม่เว้นว่าง',
    ja: 'このフィールドを空白にすることはできません'
  },
  請輸入提款總額: {
    en: 'Please enter the total withdrawal amount',
    th: 'กรุณากรอกจำนวนเงินที่ถอนทั้งหมด',
    ja: '引き出し総額を入力してください'
  },
  賬戶號碼: {
    en: 'account number',
    th: 'หมายเลขบัญชี',
    ja: '口座番号'
  },
  維護中: {
    en: 'In maintenance',
    th: 'ในการบำรุงรักษา',
    ja: 'メンテナンス中'
  },
  顯示第: {
    en: 'Show',
    th: 'แสดง',
    ja: '表示'
  },
  '項結果，': {
    en: 'result，',
    th: 'ผลลัพธ์，',
    ja: '結果，'
  },
  共: {
    en: 'Total',
    th: 'รวม',
    ja: '合計'
  },
  項: {
    en: 'item',
    th: 'สิ่งของ',
    ja: '項目'
  },
  操作成功: {
    en: 'Successful operation',
    th: 'การดำเนินงานที่ประสบความสำเร็จ',
    ja: '正常な操作'
  },
  轉出轉入不能相同: {
    en: 'Transfer out and transfer in cannot be the same',
    th: 'การโอนเข้า - ออกและการโอนเข้าต้องไม่เหมือนกัน',
    ja: '転送と転送を同じにすることはできません'
  },
  最低取款: {
    en: 'Minimum withdrawal',
    th: 'ถอนขั้นต่ำ',
    ja: '最低引き出し額'
  },
  '查詢日期區間(三個月)': {
    en: 'Query date range (three months)',
    th: 'ช่วงวันที่ของการสืบค้น (สามเดือน)',
    ja: 'クエリの日付範囲（3か月）'
  },
  確認: {
    en: 'confirm',
    th: 'ยืนยัน',
    ja: '確認'
  },
  取款金額: {
    en: 'Withdrawal amount',
    th: 'จำนวนเงินที่ถอน',
    ja: '引き出し額'
  },
  取款點數: {
    en: 'Withdrawal points',
    th: 'ถอนคะแนน',
    ja: '引き出しポイント'
  },
  銀行帳號: {
    en: 'Bank account',
    th: 'บัญชีธนาคาร',
    ja: '銀行口座'
  },
  電子郵件: {
    en: 'e-mail',
    th: 'อีเมล์',
    ja: 'Eメール'
  },
  聯繫電話: {
    en: 'contact number',
    th: 'เบอร์ติดต่อ',
    ja: '連絡先番号'
  },
  今天: {
    en: 'today',
    th: 'วันนี้',
    ja: '今日'
  },
  昨天: {
    en: 'yesterday',
    th: 'เมื่อวานนี้',
    ja: '昨日'
  },
  本周: {
    en: 'This week',
    th: 'ในสัปดาห์นี้',
    ja: '今週'
  },
  上週: {
    en: 'Last week',
    th: 'อาทิตย์ที่แล้ว',
    ja: '先週'
  },
  '確認是否轉帳?': {
    en: 'Confirm whether to transfer?',
    th: 'ยืนยันว่าจะโอน?',
    ja: '転送するかどうかを確認しますか？'
  },
  '確認是否全轉至主錢包?': {
    en: 'Is the confirmation all transferred to the main wallet?',
    th: 'การยืนยันทั้งหมดถูกโอนไปยังกระเป๋าเงินหลักหรือไม่?',
    ja: 'すべての確認はメインウォレットに転送されますか？'
  },
  '確認是否提款?': {
    en: 'Confirm whether to withdraw?',
    th: 'ยืนยันว่าจะถอน?',
    ja: '撤回するかどうかを確認しますか？'
  },
  '確認是否存款?': {
    en: 'Confirm whether to deposit?',
    th: 'ยืนยันว่าจะฝาก?',
    ja: '入金するかどうか確認しますか？'
  },
  已完成: {
    en: 'completed',
    th: 'เสร็จสมบูรณ์',
    ja: '完了'
  },
  已取消: {
    en: 'Cancelled',
    th: 'ยกเลิก',
    ja: 'キャンセル'
  },
  已拒絕: {
    en: 'rejected',
    th: 'ปฏิเสธ',
    ja: '拒否されました'
  },
  未審核: {
    en: 'Unreviewed',
    th: 'ยังไม่ได้ตรวจสอบ',
    ja: '未レビュー'
  },
  審核通過: {
    en: 'examination passed',
    th: 'ผ่านการตรวจสอบแล้ว',
    ja: '試験に合格'
  },
  審核未通過: {
    en: 'Audit failed',
    th: 'การตรวจสอบล้มเหลว',
    ja: '監査に失敗しました'
  },
  已匯款: {
    en: 'Remitted',
    th: 'ส่งแล้ว',
    ja: '送金'
  },
  已退款: {
    en: 'refunded',
    th: 'คืนเงินแล้ว',
    ja: '返金'
  },
  已逾期: {
    en: 'Overdue',
    th: 'ค้างชำระ',
    ja: '延滞'
  },
  '已逾期 收到入金': {
    en: 'The deposit has been overdue',
    th: 'เงินฝากเกินกำหนดชำระแล้ว',
    ja: 'デポジットが遅れています'
  },
  未完成: {
    en: 'undone',
    th: 'ยกเลิก',
    ja: '元に戻す'
  },
  轉帳中: {
    en: 'Transferring',
    th: 'กำลังโอน',
    ja: '転送'
  },
  交易中: {
    en: 'in transaction',
    th: 'ในการทำธุรกรรม',
    ja: 'トランザクション中'
  },
  交易錯誤: {
    en: 'Transaction error',
    th: 'ข้อผิดพลาดในการทำธุรกรรม',
    ja: 'トランザクションエラー'
  },
  未上分: {
    en: 'Not scored',
    th: 'ไม่ได้คะแนน',
    ja: 'スコアなし'
  },
  處理中: {
    en: 'Processing',
    th: 'กำลังประมวลผล',
    ja: '処理'
  },
  金額不足: {
    en: 'Insufficient amount',
    th: 'จำนวนเงินไม่เพียงพอ',
    ja: '量が足りない'
  },
  金額超出: {
    en: 'Amount exceeds',
    th: 'จำนวนเงินเกิน',
    ja: '金額が超過'
  },
  沒有任何入帳: {
    en: 'No account',
    th: 'ไม่มีบัญชี',
    ja: 'アカウントなし'
  },
  監控到此地址有錢入帳: {
    en: 'Monitoring to this address has money into the account',
    th: 'การตรวจสอบไปยังที่อยู่นี้มีเงินเข้าบัญชี',
    ja: 'このアドレスへの監視はアカウントにお金を持っています'
  },
  金額需以50為倍數: {
    en: 'The amount must be a multiple of 50',
    th: 'จำนวนต้องเป็นผลคูณของ 50',
    ja: '金額は50の倍数でなければなりません'
  },
  金額需以100為倍數: {
    en: 'The amount must be a multiple of 100',
    th: 'จำนวนต้องเป็นผลคูณของ 100',
    ja: '金額は100の倍数でなければなりません'
  },
  下注單號: {
    en: 'Bet number',
    th: 'หมายเลขเดิมพัน',
    ja: 'ベット番号'
  },
  下注時間: {
    en: 'Betting time',
    th: 'เวลาเดิมพัน',
    ja: '賭け時間'
  },
  下注資料: {
    en: 'Betting information',
    th: 'ข้อมูลการเดิมพัน',
    ja: '賭け情報'
  },
  '輸/贏': {
    en: 'Win/loss',
    th: 'ชนะ / แพ้',
    ja: '勝ち/負け'
  },
  '搜尋遊戲...': {
    en: 'Search game...',
    th: 'ค้นหาเกม ...',
    ja: '検索ゲーム...'
  },
  優惠名稱: {
    en: 'Offer name',
    th: 'ชื่อข้อเสนอ',
    ja: 'オファー名'
  },
  沒有匹配結果: {
    en: 'No matching results',
    th: 'ไม่มีผลลัพธ์ที่ตรงกัน',
    ja: '一致する結果はありません'
  },
  額度補單: {
    en: 'Replenishment order',
    th: 'คำสั่งเติมเต็ม',
    ja: '補充注文'
  },
  了解更多: {
    en: 'learn more',
    th: 'เรียนรู้เพิ่มเติม',
    ja: 'もっと詳しく知る'
  },
  幣別: {
    en: 'Currency',
    th: 'สกุลเงิน',
    ja: '通貨'
  }
};
/**
 *
let trElement=document.querySelectorAll('tr')
var object={};
for(let i=0;i<trElement.length;i++){
    if(i<3)continue;
    const tdsElement=trElement[i].querySelectorAll('td')
    let keyname=tdsElement[1].innerText;
    if(!keyname)continue;
    keyname=keyname.trim();
    object[keyname]={};
    object[keyname]['en']=tdsElement[2].innerText.trim();
    object[keyname]['th']=tdsElement[3].innerText.trim();
    object[keyname]['ja']=tdsElement[4].innerText.trim();

}
console.log(JSON.stringify(object,null,4))
 */
