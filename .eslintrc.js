module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  plugins: [
  ],
  // add your custom rules here
  rules: {
    eqeqeq: 'off',
    semi: ['error', 'always'],
    'vue/require-prop-types': 'off',
    'vue/no-v-html': 'off',
    'no-lonely-if': 'off'
  }
};
