const convertFixedTimezoneString = (value) => {
  const offset = parseInt(value.slice(0, 3));
  return offset;
};

const fixedTimezone = '+08:00';
const defaultTimezoneOffset = convertFixedTimezoneString(fixedTimezone);

const formatDate = (
  date,
  { isStartOfDate = false, isEndOfDate = false, isForApi = false } = {}
) => {
  if (isStartOfDate === true) {
    date.setHours(0, 0, 0, 0);
  } else if (isEndOfDate === true) {
    date.setHours(23, 59, 59, 999);
  }

  if (isForApi === true) {
    return new Date(
      date
        .toString()
        .substr(0, 25)
        .concat(fixedTimezone)
    );
  } else {
    return date;
  }
};

export const getDisplayDateString = (
  specificDate = new Date(),
  {
    isReturnTimeSection = false,
    dateSaparate = '-',
    timeSaparate = ':',
    noYearFormat = false,
    noSecondFormat = false
  } = {}
) => {
  const date = [
    specificDate.getFullYear(),
    `${specificDate.getMonth() + 1}`.padStart(2, 0),
    `${specificDate.getDate()}`.padStart(2, 0)
  ];

  // MM, DD
  if (noYearFormat === true) {
    date.shift();
  }

  if (isReturnTimeSection === true) {
    const time = [
      `${specificDate.getHours()}`.padStart(2, 0),
      `${specificDate.getMinutes()}`.padStart(2, 0),
      `${specificDate.getSeconds()}`.padStart(2, 0)
    ];

    // HH, MM
    if (noSecondFormat === true) {
      time.pop();
    }

    return `${date.join(dateSaparate)} ${time.join(timeSaparate)}`;
  } else {
    return `${date.join(dateSaparate)}`;
  }
};

export const getISODateString = (specificDate = new Date()) => {
  const ISODate = specificDate.toISOString();
  const removePart = ISODate.substr(ISODate.length - 5, ISODate.length);
  const date = ISODate.replace(removePart, '+00:00');

  return date;
};

export const getDateByTimezone = (
  specificDate = new Date(),
  {
    timezoneOffset = defaultTimezoneOffset,
    isStartOfDate = false,
    isEndOfDate = false,
    isForApi = false
  } = {}
) => {
  const target = new Date(specificDate);

  const offset = (target.getTimezoneOffset() + timezoneOffset * 60) * 60 * 1000;

  target.setTime(target.getTime() + offset);

  if (isStartOfDate === true) {
    return formatDate(target, { isStartOfDate, isForApi });
  } else if (isEndOfDate === true) {
    return formatDate(target, { isEndOfDate, isForApi });
  } else if (isForApi === true) {
    return formatDate(target, { isForApi });
  } else {
    return target;
  }
};

// Common time libs

export const getFirstDateOfThisMonth = (
  isForApi = false,
  specificDate = new Date()
) => {
  const target = getDateByTimezone(specificDate);
  const result = new Date(target.getFullYear(), target.getMonth(), 1);
  if (isForApi === true) {
    return formatDate(result, { isForApi });
  } else {
    return result;
  }
};

export const getPreviousDateByDays = (
  previousDays,
  { isStartOfDate = false, isEndOfDate = false },
  isForApi = false,
  specificDate = new Date()
) => {
  const target = getDateByTimezone(specificDate, {
    isStartOfDate,
    isEndOfDate
  });
  target.setDate(target.getDate() - previousDays);
  if (isForApi === true) {
    return formatDate(target, { isForApi });
  } else {
    return target;
  }
};

export const getPreviousDateByMonthes = (
  previousMonths,
  isForApi = false,
  specificDate = new Date()
) => {
  const target = new Date(specificDate.getTime());
  target.setMonth(target.getMonth() - previousMonths);
  if (isForApi === true) {
    return formatDate(target, { isForApi });
  } else {
    return target;
  }
};

export const getDateWithStartTime = (
  isForApi = false,
  specificDate = new Date()
) => {
  const target = getDateByTimezone(specificDate);
  target.setHours(0, 0, 0, 0);
  if (isForApi === true) {
    return formatDate(target, { isForApi });
  } else {
    return target;
  }
};

export const getDateWithEndTime = (
  isForApi = false,
  specificDate = new Date()
) => {
  const target = getDateByTimezone(specificDate);
  target.setHours(23, 59, 59, 999);
  if (isForApi === true) {
    return formatDate(target, { isForApi });
  } else {
    return target;
  }
};

export const getWeekdayOfWeek = (
  weekday,
  isForApi = false,
  specificDate = new Date()
) => {
  const target = getDateByTimezone(specificDate);
  const targetWeekday = target.getDay() === 0 ? 7 : target.getDay();
  const result = new Date(
    target.setDate(target.getDate() - (targetWeekday - weekday))
  );
  if (isForApi === true) {
    return formatDate(result, { isForApi });
  } else {
    return result;
  }
};

export const getFirstDateOfLastMonth = (
  isForApi = false,
  specificDate = new Date()
) => {
  const target = getDateByTimezone(specificDate);
  const result = new Date(target.getFullYear(), target.getMonth() - 1, 1);
  if (isForApi === true) {
    return formatDate(result, { isForApi });
  } else {
    return result;
  }
};

export const getLastDateOfLastMonth = (
  isForApi = false,
  specificDate = new Date()
) => {
  const target = getDateByTimezone(specificDate);
  target.setDate(0);
  if (isForApi === true) {
    return formatDate(target, { isForApi });
  } else {
    return target;
  }
};
