export default {
  account: /^[a-zA-Z0-9]{4,10}$/,
  password: /^(?!([^a-zA-Z]+|\D+)$)[a-zA-Z0-9]{6,13}$/,
  // phone: /^(5|6|8|9)\d{7}$/,
  phone: /^\d{11}$/,
  imgCode: /^[\d\D]{4}$/
};
