import Vue from 'vue';
import { mapFields } from 'vuex-map-fields';

Vue.mixin({
  computed: {
    ...mapFields('lang', ['selectLang', 'langData'])
  },
  methods: {
    getLang (name) {
      if (this.selectLang && this.langData[name] && this.langData[name][this.selectLang]) {
        return this.langData[name][this.selectLang];
      }
      return name;
    },
    simpleDateFormat (data) {
      if (!data) {
        return;
      }
      const date = new Date(data);
      const year = date.getFullYear();
      const month = (date.getMonth() + 1).toString().padStart(2, 0);
      const day = date.getDate().toString().padStart(2, 0);
      const hour = date.getHours().toString().padStart(2, 0);
      const minute = date.getMinutes().toString().padStart(2, 0);
      const second = date.getSeconds().toString().padStart(2, 0);

      return [year, month, day].join('-') + ' ' + [hour, minute, second].join(':');
    },
    copyText (targetId) {
      const node = this.$el.querySelector(targetId);
      const selection = window.getSelection();
      const range = document.createRange();
      range.selectNodeContents(node);
      selection.removeAllRanges();
      selection.addRange(range);
      document.execCommand('Copy');
      this.$store.dispatch('setAlertMessage', this.getLang('複製成功'));
      this.$bvModal.show('alertModal');
      selection.removeRange(range);
    }
  }
});
Vue.filter('toFixed', function (value, point) {
  return (value * 1).toFixed(point || 2);
});
