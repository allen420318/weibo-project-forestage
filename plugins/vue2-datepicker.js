import Vue from 'vue';
import DatePicker from 'vue2-datepicker';
import 'vue2-datepicker/index.css';
import 'vue2-datepicker/locale/ja';
import 'vue2-datepicker/locale/en.js';
import 'vue2-datepicker/locale/zh-tw.js';
import 'vue2-datepicker/locale/th.js';

Vue.component('date-picker', DatePicker);
