const isDev = process.env.NODE_ENV === 'development';
export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  ssr: false,
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'static',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title: '歡迎蒞臨::微博帝國::',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { 'http-equiv': 'X-UA-Compatible', content: 'IE=edge' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/images/favicon.ico' }
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    '~/static/css/offcanvas.css',
    '~/static/css/marquee.css',
    '~/static/css/index.css',
    '~/static/fontawesome.5.14.0/css/all.css'
  ],

  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    { src: '~/plugins/fragment.js' },
    { src: '~/plugins/common.js' },
    { src: '~/plugins/mock.js' },
    { src: '~/plugins/vue2-datepicker.js' },
    { src: '~/plugins/vee-validate.js' }
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    'bootstrap-vue/nuxt',
    'cookie-universal-nuxt'
  ],
  router: {
    base: '/'
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  env: {
    baseUrl: (process.env.baseURL || '') + '/api/'
  },
  server: {
    port: process.env.PORT || 3000, // default: 3000
    host: '0.0.0.0' // default: localhost
    // https: {
    //   key: fs.readFileSync(`${__dirname}/../cert.key`),
    //   cert: fs.readFileSync(`${__dirname}/../cert.crt`)
    // }
  },
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
    parallel: isDev,
    hardSource: isDev,
    cache: isDev,
    babel: {
      compact: true
    }
  },
  devServer: {
    disableHostCheck: true
  },
  publicRuntimeConfig: {
    baseURL: process.env.baseURL || ''
  }
};
